import { dbconn } from "../src/connection";
import { Account } from "../src/entities";
import { AccountDAO } from "../src/daos/account-dao";
import { AccountDaoDB } from "../src/daos/account-dao-impl";

const accountDAO:AccountDAO = new AccountDaoDB();

const testAccount:Account = new Account(0,'testing', 500, 1);

test("Create new account", async ()=>{
    const result:Account = await accountDAO.createAccount(testAccount);
    expect(result.accountId).not.toBe(0);
});

test("Get all acounts from client", async ()=>{
    let account1:Account = new Account(0,'testing',400,2);
    let account2:Account = new Account(0,'testing',600,2);
    await accountDAO.createAccount(account1);
    await accountDAO.createAccount(account2);
    const accounts:Account[] = await accountDAO.getAllAccountsByClient(account1.clientId);
    expect (accounts.length).toBeGreaterThan(1);
});

test("Get account by account id", async ()=>{
    let account:Account = new Account(0,'testing',222,2);
    account = await accountDAO.createAccount(account);
    let retrievedAccount = await accountDAO.getAccountById(account.accountId);
    expect(retrievedAccount.balance).toBe(account.balance);
})
test("update account", async () => {
    let account:Account = new Account(0,'testing',333,2);
    account = await accountDAO.createAccount(account);
    account.balance = 444;
    account = await accountDAO.updateAccount(account);
    const updatedAccount = await accountDAO.getAccountById(account.accountId);
    expect(updatedAccount.balance).toBe(444);
})

test("delete account by account id", async ()=>{
    let account:Account = new Account(0,'testing',1234,2);
    account = await accountDAO.createAccount(account);
    const result:boolean = await accountDAO.deleteAccountById(account.accountId);
    expect(result).toBeTruthy()
})

afterAll(async()=>{
    dbconn.end()
})