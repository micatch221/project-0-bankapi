import { dbconn } from "../src/connection";
import { ClientDAO } from "../src/daos/client-dao";
import { ClientDaoDB } from "../src/daos/client-dao-impl";
import { Client } from "../src/entities";

const clientDAO:ClientDAO = new ClientDaoDB();

const testClient:Client = new Client(0,'Luke', 'Skywalker');

test("create a client", async ()=>{
    const result:Client = await clientDAO.createClient(testClient);
    expect(result.clientId).not.toBe(0);
});

test("get all clients", async ()=>{
    const clients:Client[] = await clientDAO.getAllClients();
    expect(clients.length).toBeGreaterThanOrEqual(2)
});

test("get client by Id", async ()=>{

    let client:Client = new Client(0,'Louis', 'Armstrong');
    client = await clientDAO.createClient(client);
    let retrievedClient:Client = await clientDAO.getClientById(client.clientId);
    expect(retrievedClient.lastName).toBe(client.lastName);
})

test("update client", async ()=>{
    let client:Client = new Client(0,"Elmers","Glue");
    client = await clientDAO.createClient(client);
    client.firstName = "Bob";
    const updatedClient = await clientDAO.updateClient(client);
    expect(updatedClient.firstName).toBe("Bob");
})

test("delete client", async ()=>{
    let client:Client = new Client(0,"Git", "Lab");
    client = await clientDAO.createClient(client);
    const result:boolean = await clientDAO.deleteClientById(client.clientId);
    expect(result).toBeTruthy()
})



afterAll(async()=>{
    dbconn.end()
})