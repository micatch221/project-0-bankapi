"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var connection_1 = require("../src/connection");
var client_dao_impl_1 = require("../src/daos/client-dao-impl");
var entities_1 = require("../src/entities");
var clientDAO = new client_dao_impl_1.ClientDaoDB();
var testClient = new entities_1.Client(0, 'Luke', 'Skywalker');
test("create a client", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, clientDAO.createClient(testClient)];
            case 1:
                result = _a.sent();
                expect(result.clientId).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("get all clients", function () { return __awaiter(void 0, void 0, void 0, function () {
    var clients;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, clientDAO.getAllClients()];
            case 1:
                clients = _a.sent();
                expect(clients.length).toBeGreaterThanOrEqual(2);
                return [2 /*return*/];
        }
    });
}); });
test("get client by Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, retrievedClient;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, 'Louis', 'Armstrong');
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                return [4 /*yield*/, clientDAO.getClientById(client.clientId)];
            case 2:
                retrievedClient = _a.sent();
                expect(retrievedClient.lastName).toBe(client.lastName);
                return [2 /*return*/];
        }
    });
}); });
test("update client", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, updatedClient;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Elmers", "Glue");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                client.firstName = "Bob";
                return [4 /*yield*/, clientDAO.updateClient(client)];
            case 2:
                updatedClient = _a.sent();
                expect(updatedClient.firstName).toBe("Bob");
                return [2 /*return*/];
        }
    });
}); });
test("delete client", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Git", "Lab");
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                return [4 /*yield*/, clientDAO.deleteClientById(client.clientId)];
            case 2:
                result = _a.sent();
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        connection_1.dbconn.end();
        return [2 /*return*/];
    });
}); });
