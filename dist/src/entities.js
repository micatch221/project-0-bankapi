"use strict";
exports.__esModule = true;
exports.Account = exports.Client = void 0;
var Client = /** @class */ (function () {
    function Client(clientId, firstName, lastName) {
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return Client;
}());
exports.Client = Client;
var Account = /** @class */ (function () {
    function Account(accountId, accountType, balance, clientId) {
        this.accountId = accountId;
        this.accountType = accountType;
        this.balance = balance;
        this.clientId = clientId;
    }
    return Account;
}());
exports.Account = Account;
