"use strict";
exports.__esModule = true;
exports.InsufficientFundsError = exports.MissingResourceError = void 0;
var MissingResourceError = /** @class */ (function () {
    function MissingResourceError(message) {
        this.description = "This error means a resource could not be found";
        this.message = message;
    }
    return MissingResourceError;
}());
exports.MissingResourceError = MissingResourceError;
var InsufficientFundsError = /** @class */ (function () {
    function InsufficientFundsError(message) {
        this.description = "This error means insufficient funds were found for the transaction";
        this.message = message;
    }
    return InsufficientFundsError;
}());
exports.InsufficientFundsError = InsufficientFundsError;
