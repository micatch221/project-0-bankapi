"use strict";
exports.__esModule = true;
exports.dbconn = void 0;
var pg_1 = require("pg");
exports.dbconn = new pg_1.Client({
    user: 'postgres',
    password: process.env.DBPASSWORD,
    database: process.env.DATABASENAME,
    port: 5432,
    host: '104.196.142.138'
});
exports.dbconn.connect();
