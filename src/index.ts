import express from 'express';
import { Account, Client } from './entities';
import { InsufficientFundsError, MissingResourceError } from './errors';
import BankService from './services/bank-service';
import { BankServiceImpl } from './services/bank-service-impl';

const app = express();
app.use(express.json());

const bankService:BankService = new BankServiceImpl();

// POST /clients => Creates a new client return a 201 status code
app.post("/clients", async (req, res) =>{
    let client:Client = req.body;
    client = await bankService.addNewClient(client);
    res.status(201);
    res.send(client);
})

// GET /clients => gets all clients return 200
app.get("/clients", async (req, res) =>{
    const clients:Client[] = await bankService.retrieveAllClients();
    res.send(clients);
});

// GET /clients/10 => get client with id of 10 return 404 if no such client exists
app.get("/clients/:id", async (req, res)=>{
    try {
        const clientId = Number(req.params.id);
        const client:Client = await bankService.retrieveClientById(clientId);
        res.send(client);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// PUT /clients/12 => updates client with id of 12 return 404 if no such client exists
app.put("/clients/:id", async (req, res)=>{
    try {
        let client:Client = req.body;
        client.clientId = Number(req.params);
        client = await bankService.modifyClient(client);
        res.status(201);
        res.send(client);
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }   
});

// DELETE /clients/15 => deletes client with the id of 15 return 404 if no such client exists return 205 if success
app.delete("/clients/:id", async (req, res)=>{
    try {
        const clientId = Number(req.params.id);
        const isDeleted = await bankService.removeClientById(clientId);
        res.status(205);
        res.send(isDeleted);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }

});

// POST /clients/5/accounts =>creates a new account for client with the id of 5
//	return a 201 status code
app.post("/clients/:id/accounts", async (req, res) =>{
    const clientId = Number(req.params.id);
    let account:Account = req.body;
    account = await bankService.addNewAccount(account, clientId);
    res.status(201);
    res.send(account);
});

//   GET /clients/7/accounts => get all accounts for client 7
//       return 404 if no client exists
app.get("/clients/:id/accounts", async (req, res)=>{
    try {
        const clientId = Number(req.params.id);
        const accounts:Account[] = await bankService.retrieveAccountsByClientId(clientId);
        res.send(accounts);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});
//   (Optional)
//    GET /accounts?amountLessThan=2000&amountGreaterThan400 => get all accounts for between 400 and 200
    
//   GET /accounts/4 => get account with id 4 
//       return 404 if no account or client exists
app.get("/accounts/:id", async (req, res)=>{
    try {
        const accountId = Number(req.params.id);
        const account:Account = await bankService.retrieveAccountById(accountId)
        res.send(account);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});
    
//   PUT /accounts/3 => update account with the id 3
//       return 404 if no account or client exists
app.put("/accounts/:id", async (req, res)=>{
    try {
        let account:Account = req.body;
        account.accountId = Number(req.params.id);
        account = await bankService.modifyAccount(account);
        res.status(201);
        res.send(account);
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }   
});
    
//   DELETE /accounts/6 => delete account 6 
//       return 404 if no account or client exists
app.delete("/accounts/:id", async (req, res)=>{
    try {
        const accountId = Number(req.params.id);
        const isDeleted = await bankService.closeAccountById(accountId);
        res.status(205);
        res.send(isDeleted);
    } catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }

});
    
//   PATCH /accounts/12/deposit => deposit given amount (Body {"amount":500} )
//      return 404 if no account exists
app.patch("/accounts/:id/deposit", async (req, res)=>{
    try {
        const amount:number = Number(req.body);
        const accountId = Number(req.params.id);
        const account:Account = await bankService.depositMoney(accountId, amount);
        res.status(201);
        res.send(account);
    } catch (error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }   
});
    
//  PATCH /accounts/12/withdraw => withdraw given amount (Body {"amount":500} )
//       return 404 if no account exists
//       return 422 if insufficient funds
app.patch("/accounts/:id/withdraw", async (req, res)=>{
    try {
        const amount:number = Number(req.body);
        const accountId = Number(req.params.id);
        const account:Account = await bankService.withdrawMoney(accountId, amount);
        res.status(201);
        res.send(account);
    } catch (error){
        if(error instanceof InsufficientFundsError){
            res.status(422);
            res.send(error);
        }
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }   
});


app.listen(3000, ()=>{console.log("Application Started")});