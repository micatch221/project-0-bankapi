import { AccountDAO } from "../daos/account-dao";
import { AccountDaoDB } from "../daos/account-dao-impl";
import { ClientDAO } from "../daos/client-dao";
import { ClientDaoDB } from "../daos/client-dao-impl";
import { Client, Account } from "../entities";
import { InsufficientFundsError } from "../errors";
import BankService from "./bank-service";

export class BankServiceImpl implements BankService{
    
    clientDao:ClientDAO = new ClientDaoDB();
    accountDao:AccountDAO = new AccountDaoDB();

    addNewClient(client: Client): Promise<Client> {
        return this.clientDao.createClient(client);
    }

    retrieveAllClients(): Promise<Client[]> {
        return this.clientDao.getAllClients();
    }

    retrieveClientById(clientId: number): Promise<Client> {
        return this.clientDao.getClientById(clientId);
    }

    modifyClient(client:Client): Promise<Client> {
        return this.clientDao.updateClient(client);
    }

    removeClientById(clientId: number): Promise<boolean> {
       return this.clientDao.deleteClientById(clientId);
    }

    addNewAccount(account: Account, clientId: number): Promise<Account> {
        account.clientId = clientId;
        return this.accountDao.createAccount(account);
    }

    retrieveAccountsByClientId(clientId: number): Promise<Account[]> {
        return this.accountDao.getAllAccountsByClient(clientId);
    }

    retrieveAccountById(accountId: number): Promise<Account> {
        return this.accountDao.getAccountById(accountId);
    }

    modifyAccount(account: Account): Promise<Account> {
        return this.accountDao.updateAccount(account);
    }

    async depositMoney(accountId:number, amount:number): Promise<Account> {
        let account:Account = await this.accountDao.getAccountById(accountId);
        account.balance += amount;
        return this.accountDao.updateAccount(account);
    }

    async withdrawMoney(accountId:number, amount: number): Promise<Account> {
        let account:Account = await this.accountDao.getAccountById(accountId);
        if (amount > account.balance){
            throw new InsufficientFundsError(`Insufficient funds were found for this transaction.`);
        }
        account.balance -= amount;
        return this.accountDao.updateAccount(account);
    }
    closeAccountById(accountId: number): Promise<boolean> {
        return this.accountDao.deleteAccountById(accountId);
    }
}