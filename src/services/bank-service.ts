import { Client } from "../entities";
import { Account } from "../entities";

export default interface BankService{

    addNewClient(client:Client):Promise<Client>;

    retrieveAllClients():Promise<Client[]>

    retrieveClientById(clientId:number):Promise<Client>;

    modifyClient(client:Client):Promise<Client>;

    removeClientById(clientId:number):Promise<boolean>;

    addNewAccount(account:Account, clientId:number):Promise<Account>;

    retrieveAccountsByClientId(clientId:number):Promise<Account[]>;

    retrieveAccountById(accountId:number):Promise<Account>;

    depositMoney(accountId:number, amount:number):Promise<Account>;

    modifyAccount(account:Account):Promise<Account>;

    withdrawMoney(accountId:number, amount:number):Promise<Account>;

    closeAccountById(accountId:number):Promise<boolean>;
}