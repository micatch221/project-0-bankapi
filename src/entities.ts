export class Client{
    constructor(
        public clientId:number,
        public firstName:string,
        public lastName:string
    ){}
}

export class Account{
    constructor(
        public accountId:number,
        public accountType:string,
        public balance:number,
        public clientId:number
    ){}
}