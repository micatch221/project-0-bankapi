export class MissingResourceError{
    
    message:string;
    description:string = "This error means a resource could not be found";

    constructor(message:string){
        this.message = message;
    }
}

export class InsufficientFundsError{

    message:string;
    description:string = "This error means insufficient funds were found for the transaction"

    constructor(message:string){
        this.message = message;
    }
}