import {Client} from 'pg';

export const dbconn = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:process.env.DATABASENAME,
    port:5432,
    host:'104.196.142.138'
});
dbconn.connect()