import { Account } from "../entities";

export interface AccountDAO{

    createAccount(account:Account):Promise<Account>;

    getAllAccountsByClient(clientID:number):Promise<Account[]>;
    getAccountsByBalance(balance:number):Promise<Account[]>;
    getAccountById(accountId:number):Promise<Account>;

    updateAccount(account:Account):Promise<Account>;
    

    deleteAccountById(accountId:number):Promise<boolean>;
}