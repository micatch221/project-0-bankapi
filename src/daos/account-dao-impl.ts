
import { Account } from "../entities";
import { AccountDAO } from "./account-dao";
import { dbconn } from "../connection";
import { MissingResourceError } from "../errors";

export class AccountDaoDB implements AccountDAO{
    
    async createAccount(account: Account): Promise<Account> {
        const sql:string = 'insert into account(account_type,balance,c_id) values($1,$2,$3) returning account_id';
        const values = [account.accountType,account.balance,account.clientId];
        const result = await dbconn.query(sql,values);
        account.accountId = result.rows[0].account_id;
        return account;
    }

    async getAllAccountsByClient(clientID: number): Promise<Account[]> {
        const sql:string = 'select * from account where c_id = $1';
        const values = [clientID];
        const result = await dbconn.query(sql,values);
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.account_id,
                row.account_type,
                row.balance,
                row.c_id
            );
            accounts.push(account);
        }
        return accounts;
    }
    getAccountsByBalance(balance: number): Promise<Account[]> {
        throw new Error("Method not implemented.");
    }
    async getAccountById(accountId: number): Promise<Account> {
        const sql:string = 'select * from account where account_id = $1';
        const values = [accountId]
        const result = await dbconn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        const row = result.rows[0];
        const account:Account = new Account(
            row.account_id,
            row.account_type,
            row.balance,
            row.c_id
        );
        return account;
    }
    async updateAccount(account: Account): Promise<Account> {
        const sql:string = 'update account set account_type=$1, balance=$2 where account_id = $3'
        const values = [account.accountType,account.balance,account.accountId];
        const result = await dbconn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${account.accountId} does not exist`);
        }
        return account;
    }

    async deleteAccountById(accountId: number): Promise<boolean> {
        const sql:string = 'delete from account where account_id=$1'
        const values = [accountId];
        const result = await dbconn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        return true
    }
    
}