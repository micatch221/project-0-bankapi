import { Client } from "../entities";
import { ClientDAO } from "./client-dao";
import { dbconn } from "../connection";
import { MissingResourceError } from "../errors";

export class ClientDaoDB implements ClientDAO{
    async createClient(client: Client): Promise<Client> {
        const sql:string = "insert into client(first_name, last_name) values ($1,$2) returning client_id";
        const values = [client.firstName, client.lastName];
        const result = await dbconn.query(sql,values);
        client.clientId = result.rows[0].client_id; 
        return client;
    }
    async getAllClients(): Promise<Client[]> {
        const sql:string = 'select * from client';
        const result = await dbconn.query(sql);
        const clients:Client[] = [];
        for(const row of result.rows){
            const client:Client = new Client(
                row.clientId,
                row.firstName,
                row.lastName);
                clients.push(client);
            }
       return clients;
    }
    async getClientById(clientID: number): Promise<Client> {
        const sql:string = 'select * from client where client_id = $1';
        const values = [clientID]
        const result = await dbconn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${clientID} does not exist`)
        }
        const row = result.rows[0];
        const client:Client = new Client(
            row.client_id,
            row.first_name,
            row.last_name);
            console.log(client);
        return client;
        
    }
    async updateClient(client: Client): Promise<Client> {
        const sql:string = 'update client set first_name=$1, last_name=$2 where client_id=$3';
        const values = [client.firstName,client.lastName,client.clientId];
        const result = await dbconn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with id ${client.clientId} does not exist`)
        }
        return client;
    }

    async deleteClientById(clientID: number): Promise<boolean> {
       const sql:string = 'delete from client where client_id=$1';
       const values = [clientID];
       const result = await dbconn.query(sql,values);
       if(result.rowCount === 0){
           throw new MissingResourceError(`The client with id ${clientID} does not exist`);
       }
       return true 
    }
    
}