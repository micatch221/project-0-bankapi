import { Client } from "../entities";

export interface ClientDAO{

    createClient(client:Client):Promise<Client>;

    getAllClients():Promise<Client[]>;
    getClientById(clientID:number): Promise<Client>;

    updateClient(client:Client):Promise<Client>;

    deleteClientById(clientID:number):Promise<boolean>;

}